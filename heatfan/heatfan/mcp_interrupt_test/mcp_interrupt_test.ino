#include <TM1637Display.h>
#include <Wire.h>
#include <Adafruit_MCP23017.h>


byte temp_setButt = 6;        // setup temperature in manual mode - GPA6 pin of MCP21307
byte temp_incButt = 5;        // increase by 1 degree of temperature in manual mode - GPA5 pin of MCP21307
byte temp_decButt = 4;        // decrease by 1 degree of temperature in manual mode - GPA4 pin of MCP21307
byte manu_modeLed = 9;

byte esp8266IntPin = 3;  // use GPIO15 of esp8266
Adafruit_MCP23017 mcp;

// define clk and dio of TM1637
#define T1_CLK 12   // use GPIO 12, define clk of TM1637 (current temperature display)
#define T1_DIO 14   // use GPIO 14, define dio of TM1637 (current temperature display)
#define T2_CLK 0    // use GPIO 0, define clk of TM1637 (setting temperature display)    ?????????????????????
#define T2_DIO 2    // use GPIO 2, define dio of TM1637 (setting temperature display)    ?????????????????????

TM1637Display display1(T1_CLK, T1_DIO);
TM1637Display display2(T2_CLK, T2_DIO);

long temp = 22;  // initial temperature
int upload_interval = 1000;   // Uploading current brightness level and brightness level by potentiometer in miliseconds.
long lastMsg = 0;
int butt_memo;  //variable for memoring of temp_setButt state

void setup() {

  Serial.begin(115200);

  pinMode(esp8266IntPin, INPUT);

  mcp.begin();   // use default address 0
  mcp.setupInterrupts(true, false, LOW);

  mcp.pinMode(temp_setButt, INPUT);      // button for interrupt
  mcp.pullUp(temp_setButt, HIGH);
  mcp.setupInterruptPin(temp_setButt, CHANGE);
  mcp.pinMode(temp_incButt, INPUT);
  mcp.pullUp(temp_incButt, HIGH);
  mcp.setupInterruptPin(temp_incButt, CHANGE);
  mcp.pinMode(temp_decButt, INPUT);
  mcp.pullUp(temp_decButt, HIGH);
  mcp.setupInterruptPin(temp_decButt, CHANGE);

  mcp.pinMode(manu_modeLed, OUTPUT);

  attachInterrupt(esp8266IntPin, handle_ButtInterrupt, FALLING); /*interrupt for setting of temperature in manual mode*/
  //This will clear interrupts on MCP prior to entering main loop
  mcp.readGPIOAB();

}

void handle_ButtInterrupt() {

  //Serial.println("Interrupt detected!");

  // Get more information from the MCP from the INT
  uint8_t pin = mcp.getLastInterruptPin();
  uint8_t val = mcp.getLastInterruptPinValue();

  if ((pin == temp_setButt) && (val == LOW)) {
    Serial.println("Please setup temperature");

    switch (butt_memo) {
      case 0: {
          butt_memo = 1;
          mcp.digitalWrite(manu_modeLed, HIGH);
        } break;
      case 1: {
          butt_memo = 0;
          mcp.digitalWrite(manu_modeLed, LOW);
        } break;
    }
  }
  Serial.println(butt_memo);
  if ((pin == temp_incButt) && (val == LOW)) {
    if (butt_memo == 1) {
      temp = temp + 1;
      //Serial.println("Temperature increase by 1.");
    }
  }
  if ((pin == temp_decButt) && (val == LOW)) {
    if (butt_memo == 1) {
      temp = temp - 1;
      //Serial.println("Temperature decrease by 1.");
    }
  }
  mcp.readGPIOAB();
}

void loop() {

  long now = millis();
  if (now - lastMsg > upload_interval) {
    lastMsg = now;
    Serial.print("Current temperature is  ");
    Serial.println(temp);
  }
}


