#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SPI.h>

/*
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>
#include <FS.h>
*/

const char* device_name = "waterworks";   // This device is waterworks.
int upload_interval = 1000;       // Uploading water depth and valve state in miliseconds.

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself

/*    --------  topic setting ----------------      */

const char* valve_set_suffix = "/valve_set";
const char* watertank_depth_set_suffix = "/watertank_depth/set";

const char* valve_state_suffix = "/valve_state";
const char* watertank_depth_suffix = "/watertank_depth";
const char* watertank_depth_alarm_suffix = "/watertank_depth/alarm";

// Pin setting

int PIN_MS5540_mclk = 9;  // MCLK pin for MS5540 sensor (Use GPIO9)
int PIN_valveOn = 10;  // pin to open valve  (Use GPIO10)
int PIN_valveOn_Led = 5;  // pin to notice valve on (Use GPIO5)
int PIN_overalarm_Led = 4;  // pin to notice overalarm  (Use GPIO4)
int PIN_shortalarm_Led = 16:  // pin to notice shortalarm (Use GPIO16)

// Global variables

WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;
float pressure, curr_depth;
char* depth_set;  //  low depth from mobile app(char* type)
float val_depth_set;  // low depth from mobile app(float type)

float watertank_depth_low = 20;  // permissible minimum watertank depth in cm unit
//float watertank_depth_top = 100;

const char* valve_state = "OFF";
const char* watertank_state = "NORMAL";

char buf_pub_topic[50];
char buf_sub_topic[50];

void setup() {
  pinMode(PIN_valveOn, OUTPUT);
  digitalWrite(PIN_valveOn, LOW);
  pinMode(PIN_valveOn_Led, OUTPUT);
  digitalWrite(PIN_valveOn_Led, LOW);
  pinMode(PIN_overalarm_Led, OUTPUT);
  digitalWrite(PIN_overalarm_Led, LOW);
  pinMode(PIN_shortalarm_Led, OUTPUT);
  digitalWrite(PIN_shortalarm_Led, LOW);

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  delay(2000);
  setup_sensor();
  delay(500);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup_sensor() {
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV32);   // divide 16MHz to communicate on 500KHZ

  /*#ifdef ESP8266
    SPI.setFrequency(500000);
    #else
    SPI.setClockDivider(SPI_CLOCK_DIV32);
    #endif
  */
  pinMode(PIN_MS5540_mclk, OUTPUT);
  delay(100);
}

void resetsensor() {
  SPI.setDataMode(SPI_MODE0);
  SPI.transfer(0x15);
  SPI.transfer(0x55);
  SPI.transfer(0x40);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");

  // Check topic of valve set and open led
  set_sub_topic(valve_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

    if (!strncmp((const char*)payload, "ON", 2)) {

      valve_state = "ON";
      Serial.println("Current state of valve is ON...");
      digitalWrite(PIN_valveOn_Led, HIGH);
      digitalWrite(PIN_valveOn, HIGH);
    }
    else if (!strncmp((const char*)payload, "OFF", 3)) {
      valve_state = "OFF";
      Serial.println("Current state of valve is OFF...");
      digitalWrite(PIN_valveOn_Led, LOW);
      digitalWrite(PIN_valveOn, LOW);
    }
  }
  // Check topic of watertank depth, and than control valve by it.
  else {
    set_sub_topic(watertank_depth_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

      depth_set = (char*)payload;
      val_depth_set = atof(depth_set);
      Serial.print("Watertank depth was set:  ");
      Serial.print(val_depth_set);
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      set_sub_topic(valve_set_suffix);
      client.subscribe(buf_sub_topic);
      set_sub_topic(watertank_depth_set_suffix);
      client.subscribe(buf_sub_topic);
      client.subscribe("common");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > upload_interval) {
    lastMsg = now;
    pressure = read_MS5540();
    curr_depth = pressure;
    Serial.print("Current depth of watertank is ");
    Serial.println(curr_depth);

    char* buf_curr_depth = new char[10];
    dtostrf(buf_depth, 5, 1, buf_curr_depth);

    //publish current depth of watertank
    set_pub_topic(watertank_depth_suffix);
    client.publish(buf_pub_topic, buf_curr_depth);

    if (curr_depth > val_depth_set) {
      digitalWrite(PIN_valveOn, LOW);
      digitalWrite(PIN_valveOn_Led, LOW);
      digitalWrite(PIN_overalarm_Led, HIGH);
      valve_state = "OFF";
      watertank_state = "OVER";
      Serial.println("Now the depth of watertank exceeded setting depth.");
    }
    else if (curr_depth < watertank_depth_low) {
      digitalWrite(PIN_shortalarm_Led, HIGH);
      watertank_state = "SHORT";
      Serial.println("Now the depth of watertank is lower than permissible minimum depth.");
    }
    else {
      digitalWrite(PIN_overalarm_Led, LOW);
      digitalWrite(PIN_shortalarm_Led, LOW);
      watertank_state = "NORMAL";
    }
    // publish valve state
    set_pub_topic(valve_state_suffix);
    client.publish(buf_pub_topic, valve_state);
    // publish watertank state
    set_pub_topic(watertank_depth_alarm_suffix);
    client.publish(buf_pub_topic, watertank_state);    
  }
  else {
    delay(500);  // Loop function takes about 300ms, so 400 ms is enough.
  }
}
void read_MS5540() {

  TCCR1B = (TCCR1B & 0xF8) | 1 ; //generates the MCKL signal
  analogWrite(PIN_MS5540_mclk, 128);
  Serial.println("HAHAHA d7kt 3leek");

  /*#ifdef __AVR__
    TCCR1B = (TCCR1B & 0xF8) | 1 ; //generates the MCKL signal
    Serial.println("HAHAHA d7kt 3leek");
    #else
    analogWriteFreq(10);
    analogWriteRange(256);
    #endif
    analogWrite(PIN_MS5540_mclk, 128);
  */
  resetsensor(); //resets the sensor - caution: afterwards mode = SPI_MODE0!

  //Calibration word 1
  unsigned int word1 = 0;
  unsigned int word11 = 0;
  SPI.transfer(0x1D); //send first byte of command to get calibration word 1
  SPI.transfer(0x50); //send second byte of command to get calibration word 1
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  word1 = SPI.transfer(0x00); //send dummy byte to read first byte of word
  word1 = word1 << 8; //shift returned byte
  word11 = SPI.transfer(0x00); //send dummy byte to read second byte of word
  word1 = word1 | word11; //combine first and second byte of word

  resetsensor();//resets the sensor

  //Calibration word 2; see comments on calibration word 1
  unsigned int word2 = 0;
  byte word22 = 0;
  SPI.transfer(0x1D);
  SPI.transfer(0x60);
  SPI.setDataMode(SPI_MODE1);
  word2 = SPI.transfer(0x00);
  word2 = word2 << 8;
  word22 = SPI.transfer(0x00);
  word2 = word2 | word22;

  resetsensor();//resets the sensor

  //Calibration word 3; see comments on calibration word 1
  unsigned int word3 = 0;
  byte word33 = 0;
  SPI.transfer(0x1D);
  SPI.transfer(0x90);
  SPI.setDataMode(SPI_MODE1);
  word3 = SPI.transfer(0x00);
  word3 = word3 << 8;
  word33 = SPI.transfer(0x00);
  word3 = word3 | word33;

  resetsensor();//resets the sensor

  //Calibration word 4; see comments on calibration word 1
  unsigned int word4 = 0;
  byte word44 = 0;
  SPI.transfer(0x1D);
  SPI.transfer(0xA0);
  SPI.setDataMode(SPI_MODE1);
  word4 = SPI.transfer(0x00);
  word4 = word4 << 8;
  word44 = SPI.transfer(0x00);
  word4 = word4 | word44;

  long c1 = word1 >> 1;
  long c2 = ((word3 & 0x3F) << 6) | ((word4 & 0x3F));
  long c3 = (word4 >> 6) ;
  long c4 = (word3 >> 6);
  long c5 = (word2 >> 6) | ((word1 & 0x1) << 10);
  long c6 = word2 & 0x3F;

  resetsensor();//resets the sensor

  //Temperature:
  unsigned int tempMSB = 0; //first byte of value
  unsigned int tempLSB = 0; //last byte of value
  unsigned int D2 = 0;
  SPI.transfer(0x0F); //send first byte of command to get temperature value
  SPI.transfer(0x20); //send second byte of command to get temperature value
  delay(35); //wait for conversion end
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  tempMSB = SPI.transfer(0x00); //send dummy byte to read first byte of value
  tempMSB = tempMSB << 8; //shift first byte
  tempLSB = SPI.transfer(0x00); //send dummy byte to read second byte of value
  D2 = tempMSB | tempLSB; //combine first and second byte of value

  resetsensor();//resets the sensor

  //Pressure:
  unsigned int presMSB = 0; //first byte of value
  unsigned int presLSB = 0; //last byte of value
  unsigned int D1 = 0;
  SPI.transfer(0x0F); //send first byte of command to get pressure value
  SPI.transfer(0x40); //send second byte of command to get pressure value
  delay(35); //wait for conversion end
  SPI.setDataMode(SPI_MODE1); //change mode in order to listen
  presMSB = SPI.transfer(0x00); //send dummy byte to read first byte of value
  presMSB = presMSB << 8; //shift first byte
  presLSB = SPI.transfer(0x00); //send dummy byte to read second byte of value
  D1 = presMSB | presLSB;

  const long UT1 = (c5 * 8) + 20224;
  const long dT = (D2 - UT1);
  const long TEMP = 200 + ((dT * (c6 + 50)) / 1024);
  const long OFF  = (c2 * 4) + (((c4 - 512) * dT) / 4096);
  const long SENS = c1 + ((c3 * dT) / 1024) + 24576;

  long PCOMP = ((((SENS * (D1 - 7168)) / 16384) - OFF) / 32) + 250;
  //  float TEMPREAL = TEMP/10;
  Serial.print("pressure =    ");
  Serial.print(PCOMP);
  Serial.println(" mbar");

  const long dT2 = dT - ((dT >> 7 * dT >> 7) >> 3);
  const float TEMPCOMP = (200 + (dT2 * (c6 + 100) >> 11)) / 10;
  Serial.print("temperature = ");
  Serial.print(TEMPCOMP);
  Serial.println(" °C");
  Serial.println("************************************");
  analogWriteFreq(1000);
  analogWriteRange(PWMRANGE);
  return PCOMP; // in mbar
}

void set_pub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}

